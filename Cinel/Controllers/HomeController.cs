﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cinel.Models;

namespace Cinel.Controllers
{
    public class HomeController : Controller
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(FormCollection frm)
        {
            if (frm["nameform"] == "modal")
            {
                if ((frm["Nomel"] != String.Empty) && (frm["Utilizador"] != String.Empty)
                          && (frm["Password"] != String.Empty) && (frm["PasswordC"] != String.Empty))
                {
                    if (frm["Password"] != frm["PasswordC"])
                    {
                        ViewBag.erro = "<br><br><div class='alert alert-danger' role='alert'>Os campos password devem der iguais</div>";
                    }
                    else
                    {
                        MVC_GuardaDado mvc = new MVC_GuardaDado
                        {
                           Nomel = frm["Nomel"],
                           Utilizador = frm["Utilizador"],
                           Password = frm["Password"],
                           PasswordC = frm["PasswordC"]
                        };
                        dc.MVC_GuardaDados.InsertOnSubmit(mvc);
                        try
                        {
                            dc.SubmitChanges();
                        }
                        catch (Exception e)
                        {
                            dc.SubmitChanges();
                        }

                        ViewBag.erro = "<br><br><div class='alert alert-success' role='alert'><p>Registo efectuado com sucesso</p></div>";
                    }
                }
                else
                {
                    ViewBag.erro = "<br><br><div class='alert alert-danger' role='alert'><p>Preencha todos os campos</p></div>";


                }
            }
            if (frm["nameform"] == "msg")
            {
                if ((String.Empty != frm["Nome"]) && (String.Empty != frm["Email"]) && (String.Empty != frm["Mensagem"]))
                {


                    messagen m = new messagen
                    {
                        nome = frm["Nome"],
                        email = frm["Email"],
                        messagem = frm["Mensagem"],
                    };

                    dc.messagens.InsertOnSubmit(m);
                    try
                    {
                        dc.SubmitChanges();
                    }
                    catch (Exception e)
                    {
                        dc.SubmitChanges();
                    }
                    ViewBag.erro = "<br><br><div class='alert alert-success' role='alert'>Mensagem enviada com sucesso</div>";
                }
                else
                {
                    ViewBag.erro = "<br><br><div class='alert alert-danger' role='alert'>Preencha todos os campos</div>";
                }
            }
                   
            return View();
        }
       

    }
}